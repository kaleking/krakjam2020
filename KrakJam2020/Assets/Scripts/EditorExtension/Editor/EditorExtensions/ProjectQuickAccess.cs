﻿

using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;


public class ProjectQuickAccess : EditorWindow
{


    private Texture texture;

    public Texture Texture
    {
        get {
            return texture;
        }

        set {
            texture = value;
        }
    }

    [MenuItem("Window/Quick Access/Panel")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(ProjectQuickAccess));
    }

    [MenuItem("Window/Quick Access/UnloadAndPlay %k")]
    public static void UnloadAndPlay()
    {
        Play();
    }


    void OnGUI()
    {
        GUILayout.Label("FO4 QUICK ACCESS PANEL.");
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button(new GUIContent("Unload and Play", "Unload all additive scenes added to Hierarchy except active scene and play appliaction in editor (unloaded scenes, will be able to load after playing)"), GUILayout.Height(20), GUILayout.Width(165)))
        {
            Play();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button(new GUIContent("Load", "Load all additive scenes added to Hierarchy."), GUILayout.Height(20), GUILayout.Width(80)))
        {
            for (int i = 0; i < EditorSceneManager.sceneCount; i++)
            {
                if (EditorSceneManager.GetSceneAt(i) != EditorSceneManager.GetActiveScene())
                {
                    EditorSceneManager.OpenScene(EditorSceneManager.GetSceneAt(i).path, OpenSceneMode.Additive);
                }
            }

        }

        if (GUILayout.Button(new GUIContent("Unload", "Unload all additive scenes added to Hierarchy except active scene"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            Unload();

        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button(new GUIContent("Scripts", (Texture)Resources.Load("Scripts/Editor/DevTools/EditorExtensions/QuickAccessIcons/GoldStar1"), "Show scripts"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            object obj = AssetDatabase.LoadAssetAtPath("Assets/Scripts/Editor", typeof(object));
            Selection.activeObject = (UnityEngine.Object)obj;
        }

        if (GUILayout.Button(new GUIContent("Scenes", (Texture)Resources.Load("Scripts/Editor/DevTools/EditorExtensions/QuickAccessIcons/BlackStar"), "Show scenes"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            object obj = AssetDatabase.LoadAssetAtPath("Assets/Scenes/Main.unity", typeof(object));
            Selection.activeObject = (UnityEngine.Object)obj;
        }
        if (GUILayout.Button(new GUIContent("Models", (Texture)Resources.Load("Scripts/Editor/DevTools/EditorExtensions/QuickAccessIcons/BlackStar"), "Show models"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            object obj = AssetDatabase.LoadAssetAtPath("Assets/Models/UI", typeof(object));
            Selection.activeObject = (UnityEngine.Object)obj;
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button(new GUIContent("Prefabs", (Texture)Resources.Load("Scripts/Editor/DevTools/EditorExtensions/QuickAccessIcons/WhiteStar"), "Show prefabs"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            object obj = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/UI", typeof(object));
            Selection.activeObject = (UnityEngine.Object)obj;
        }

        if (GUILayout.Button(new GUIContent("Resources", (Texture)Resources.Load("Scripts/Editor/DevTools/EditorExtensions/QuickAccessIcons/Cloud"), "Show resources"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            object obj = AssetDatabase.LoadAssetAtPath("Assets/Resources/Audio", typeof(object));
            Selection.activeObject = (UnityEngine.Object)obj;
        }
        if (GUILayout.Button(new GUIContent("Animations", (Texture)Resources.Load("Scripts/Editor/DevTools/EditorExtensions/QuickAccessIcons/BlackStar"), "Show animations"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            object obj = AssetDatabase.LoadAssetAtPath("Assets/Animations/Simulation", typeof(object));
            Selection.activeObject = (UnityEngine.Object)obj;
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button(new GUIContent("Sprites", (Texture)Resources.Load("Scripts/Editor/DevTools/EditorExtensions/QuickAccessIcons/GreenHeart1"), "Show sprites"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            object obj = AssetDatabase.LoadAssetAtPath("Assets/Sprites/Characters", typeof(object));
            Selection.activeObject = (UnityEngine.Object)obj;
        }

        if (GUILayout.Button(new GUIContent("Str.Assets", (Texture)Resources.Load("Scripts/Editor/DevTools/EditorExtensions/QuickAccessIcons/BlackHeart"), "Show streaming assets"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            object obj = AssetDatabase.LoadAssetAtPath("Assets/StreamingAssets/Data", typeof(object));
            Selection.activeObject = (UnityEngine.Object)obj;
        }

        if (GUILayout.Button(new GUIContent("Del Pl.Prefs", "Delete all player prefs"), GUILayout.Height(20), GUILayout.Width(80)))
        {
            PlayerPrefs.DeleteAll();
        }
    }

  

    private static void Play()
    {
        Unload();
        EditorApplication.isPlaying = true;
    }

    private static void Unload()
    {
        for (int i = 0; i < EditorSceneManager.sceneCount; i++)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            if (EditorSceneManager.GetSceneAt(i) != EditorSceneManager.GetActiveScene())
            {
                EditorSceneManager.CloseScene(EditorSceneManager.GetSceneAt(i), false);
            }
        }
    }
}
