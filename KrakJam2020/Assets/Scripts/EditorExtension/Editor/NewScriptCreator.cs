﻿namespace editorextension
{
    using UnityEditor;
    using System.IO;

    public class NewScriptCreator
    {
        private const string templatePath = "Assets/Scripts/EditorExtension/Editor";

        [MenuItem("Assets/Create/Create custom C# script %l")]
        public static void CreateCustomScript(MenuCommand cmd)
        {
            CreateScriptAsset(templatePath + "/TemplateScript.cs.txt", "NewScript.cs");
        }

        static void CreateScriptAsset(string templatePath, string destName)
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(templatePath, destName);
        }
    }
}

