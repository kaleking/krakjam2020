﻿namespace krakjam2020 {
    using System.Collections;
    using UnityEngine;

    [RequireComponent(typeof(Animator))]
    public class AnimatorDalayStarter : MonoBehaviour {
        public float dalay = 1;

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(dalay);
            GetComponent<Animator>().enabled = true;
        }
    }
}