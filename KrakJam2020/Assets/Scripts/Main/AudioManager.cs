﻿namespace krakjam2020 {
    using System.Collections;
    using UnityEngine;
    using UnityEngine.AddressableAssets;
    using UnityEngine.ResourceManagement.AsyncOperations;

    public class AudioManager : MonoBehaviour {
        const string OST_PATH = "ost/Corpse_Assembler_OST_{0}.wav";
        [SerializeField] int ostCount = 5;
        private AudioSource source;

        public void SetOST()
        {
            int index = Random.Range(1, ostCount+1);
            Addressables.LoadAssetAsync<AudioClip>(string.Format(OST_PATH, index)).Completed += onInstantiated;
        }

        public void Stop()
        {
            StopAllCoroutines();
            StartCoroutine(Mute());
        }

        private void onInstantiated(AsyncOperationHandle<AudioClip> obj)
        {
            AudioClip clip = obj.Result;
            source.clip = clip;
            source.Play();
            source.loop = true;
            StopAllCoroutines();
            StartCoroutine(InitializeAudio());
        }

        private IEnumerator InitializeAudio()
        {
            AudioSource source = GetComponent<AudioSource>();
            source.volume = 0;
            float step = 0;
            while (step <= 1)
            {
                source.volume = step;
                step += Time.deltaTime;
                yield return null;
            }
        }

        private IEnumerator Mute()
        {
            AudioSource source = GetComponent<AudioSource>();
            float step = 1;
            while (step >= 0)
            {
                source.volume = step;
                step -= Time.deltaTime/2;
                yield return null;
            }
        }

        private void Awake()
        {
            source = GetComponent<AudioSource>();
        }

        private void Start()
        {
            GameManager.Instance.OnGameOver += OnGameOver;
        }

        private void OnGameOver()
        {
            Stop();
        }
    }
}