﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public static CameraMover Instance;
    [SerializeField] private float time;
    private Vector3 startingPosition;
    private Animator animator;
    [Header("Sfx")]
    [SerializeField] private SfxTrigger zoomOutSfx;
    [SerializeField] private SfxTrigger zoomInSfx;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        animator = transform.parent.GetComponent<Animator>();
        startingPosition = this.transform.position;
    }

    private void Start()
    {
        GameManager.Instance.OnGameStart += () => StartCoroutine(ZoomOutDelayed(2f));
    }

#if UNITY_EDITOR
    //private void OnGUI()
    //{
    //    if (GUI.Button(new Rect(10, 10, 100, 100), "Move"))
    //    {
    //        var current = transform.position;
    //        current.x -= 12;
    //        StopAllCoroutines();
    //        StartCoroutine(LerpCameraPosition(current));
    //    }

    //    if (GUI.Button(new Rect(150, 10, 100, 100), "StartAgain"))
    //    {
    //        ReturnToOriginalPosition();
    //    }
    //}
#endif


    public void StartEndGameAnimation()
    {
        animator.enabled = true;
        transform.position = new Vector3(-46.99155f, 15.23003f, 27.18473f);
        animator.SetTrigger("EndGame");
    }

    public void ReturnToOriginalPosition()
    {
        StartCoroutine(LerpCameraPosition(startingPosition, true));
    }

    public void MoveCamera(int distance = 12)
    {
        var current = transform.position;
        current.x -= distance;
        StopAllCoroutines();
        StartCoroutine(LerpCameraPosition(current));
    }

    private IEnumerator LerpCameraPosition(Vector3 destination, bool zoomInAfter = false)
    {
        float currentLerpTime = 0f;
        var startingPosition = transform.position;
        while (currentLerpTime <= 1f)
        {
            currentLerpTime += 1/time * Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, destination, currentLerpTime);
            yield return null;
        }
        transform.position = destination;

        if (zoomInAfter)
            ZoomInCamera();
    }

    public void ZoomInCamera()
    {
        if (animator.enabled == false)
            animator.enabled = true;

        StartCoroutine(Animate("ZoomedIn", true));
        zoomOutSfx.PlaySfx();
    }

    public void ZoomOutCamera()
    {
        if (animator.enabled == false)
            animator.enabled = true;

        StartCoroutine(Animate("ZoomedIn", false));
        zoomInSfx.PlaySfx();
    }

    private IEnumerator ZoomOutDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);
        ZoomOutCamera();
    }

    private IEnumerator Animate(string parameter, bool value)
    {
        animator.SetBool(parameter, value);
        yield return new WaitForSeconds(2f);
        animator.enabled = false;
    }
}
