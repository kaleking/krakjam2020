﻿namespace krakjam2020 {
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.AI;

    public class EndGameController : MonoBehaviour
    {
        [SerializeField] Transform endPoint = null;
        [SerializeField] GameObject EndCamera = null;
        public void EndGame()
        {
            StartCoroutine(StartEndGame());
        }

        private IEnumerator StartEndGame()
        {
            GetComponent<NavMeshAgent>().SetDestination(endPoint.position);
            yield return new WaitForSeconds(3);
            GetComponent<NavMeshAgent>().enabled = false;
            GetComponent<Animator>().SetTrigger("EndGame");
            yield return new WaitForSeconds(2f);
            Camera.main.enabled = false;
           CameraMover.Instance.StartEndGameAnimation();
            EndCamera.SetActive(true);
            yield return new WaitForSeconds(10);
            //GetComponent<Animator>().SetTrigger("JumpToRespawn");
            //EndCamera.SetActive(false);
            //Camera.main.enabled = true;
            Application.Quit();


        }
    }
}