﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] Transform corpsHolder = null;
    [SerializeField] private SfxTrigger gameOverSfx;
    [SerializeField] private SfxTrigger startJingle;
    [SerializeField] private SfxTrigger water;
    public static GameManager Instance;
    private CameraMover cameraMover;
    private PathMover pathMover;

    public event Action OnGameStart;

    public event Action OnGameOver;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        cameraMover = FindObjectOfType<CameraMover>();
        pathMover = FindObjectOfType<PathMover>();
    }

    private void Start()
    {
        startJingle.PlaySfx();
    }

    public void InvokeGameStart()
    {
        OnGameStart?.Invoke();
        water.PlaySfx();
    }


    [ContextMenu("Test game over")]
    public void InvokeGameOver()
    {
        OnGameOver?.Invoke();
        gameOverSfx.PlaySfx();
        StartCoroutine(GameOverSequence());
    }

    private IEnumerator GameOverSequence()
    {
        pathMover.Die();
        yield return new WaitForSeconds(1f);
        cameraMover.ReturnToOriginalPosition();
    }
}
