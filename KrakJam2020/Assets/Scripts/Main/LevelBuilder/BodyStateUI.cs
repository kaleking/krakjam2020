﻿
using krakjam2020.levelgenerator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyStateUI : MonoBehaviour
{
    [SerializeField] private LimbHUD[] limbs;
    [SerializeField] private GameObject core;
    [SerializeField] private Color assigned;
    [SerializeField] private Color unassigned;

    public GameObject Core => core;

    public void SetLimb(LimbType limbType, bool isAssigned)
    {
        for (int i = 0; i < limbs.Length; i++)
        {
            if (limbs[i].LimbType == limbType)
            {
                limbs[i].Sprite.color = isAssigned? assigned : unassigned;
                break;

            }
        }
    }
}
