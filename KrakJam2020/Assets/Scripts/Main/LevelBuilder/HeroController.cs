﻿namespace krakjam2020
{
    using krakjam2020.levelgenerator;
    using System.Collections;
    using UnityEngine;

    public class HeroController : MonoBehaviour
    {
        [SerializeField] HeroPart[] parts = null;
        private BodyStateUI bodyStateUI;
        [SerializeField] private SfxTrigger pickUpSfx;

        private void Awake()
        {
            bodyStateUI = FindObjectOfType<BodyStateUI>();
            bodyStateUI.Core.SetActive(true);
        }

        public void SetupHero(TargetsDatas[] targetsDatas)
        {
            for (int i = 0; i < parts.Length; i++)
            {
                parts[i].part.SetActive(true);
                bodyStateUI.SetLimb(parts[i].limbType, true);
                for (int j = 0; j < targetsDatas.Length; j++)
                {
                    if (targetsDatas[j].limbType == parts[i].limbType)
                    {
                        parts[i].part.SetActive(false);
                        bodyStateUI.SetLimb(parts[i].limbType, false);
                    }
                }
            }
        }

        public void AttachLimb(LimbType limbType)
        {
            for (int i = 0; i < parts.Length; i++)
            {
                if (parts[i].limbType == limbType)
                {
                    parts[i].part.SetActive(true);
                    bodyStateUI.SetLimb(parts[i].limbType, true);
                    pickUpSfx.PlaySfx();
                    break;
                }
            }
        }
    }
}