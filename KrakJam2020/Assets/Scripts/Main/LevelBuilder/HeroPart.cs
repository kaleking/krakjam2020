﻿namespace krakjam2020
{
    using krakjam2020.levelgenerator;
    using System;
    using UnityEngine;

    [Serializable]
    public class HeroPart
    {
        public GameObject part;
        public LimbType limbType;
    }
}