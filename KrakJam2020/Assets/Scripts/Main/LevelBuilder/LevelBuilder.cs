﻿namespace krakjam2020.levelgenerator
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.AddressableAssets;
    using UnityEngine.ResourceManagement.AsyncOperations;

    public class LevelBuilder : MonoBehaviour
    {
        public static LevelBuilder Instance;

        [SerializeField] Player player = null;
        [SerializeField] AudioManager audioManager = null;
        [SerializeField] Transform corpsHolder = null;
        [SerializeField] PlateController plateController = null;
        const string RIVER_TILES = "river/L_{0}.prefab";
        public string seed = "";
        public bool useFixedTile = false;
        public int FixedTileNumber = 0;
        public int tilesOnGame = 4;
        public int tilesPrefabCount = 4;
        public List<Tile> spawnedTiles = new List<Tile>();

        [ContextMenu("Spawn")]
        public void SpawnTiles()
        {
            StopAllCoroutines();
            StartCoroutine(Spawn());
        }

        bool isInstatiateCompleted = false;
        private IEnumerator Spawn()
        {
            yield return new WaitForEndOfFrame();
            foreach (Tile tile in spawnedTiles)
            {
                Destroy(tile.gameObject);
            }
            spawnedTiles.Clear();
            yield return new WaitForEndOfFrame();
            Random.InitState(seed.GetHashCode());
            for (int i = 0; i < tilesOnGame; i++)
            {
                int nextTile = (useFixedTile == true) ? FixedTileNumber : Random.Range(0, tilesPrefabCount);
                Addressables.InstantiateAsync(string.Format(RIVER_TILES, nextTile)).Completed += onInstantiated;
                yield return new WaitUntil(() => isInstatiateCompleted == true);
                isInstatiateCompleted = false;
            }
            SetupTargets();
            SetupOST();
        }

        private void onInstantiated(AsyncOperationHandle<GameObject> obj)
        {
            Tile tile = obj.Result.gameObject.GetComponent<Tile>();
            spawnedTiles.Add(obj.Result.gameObject.GetComponent<Tile>());
            float shift = 0 - (spawnedTiles.Count - 1) * 12;
            obj.Result.transform.position = new Vector3(shift, 0, 0);
            SetupEnviro(tile);
            SetupObstacles(tile);
            SetupRotation(tile);
            isInstatiateCompleted = true;
        }

        private void SetupOST()
        {
            audioManager.SetOST();
        }

        private void SetupEnviro(Tile tile)
        {
            int index = Random.Range(0, tile.NumberOfEnvironments);
            tile.SetEnvironment(index);
        }

        private void SetupObstacles(Tile tile)
        {
            int index = Random.Range(0, tile.NumberOfObstacles);
            tile.SetObstacle(index);
            index = Random.Range(0, tile.NumberOfObstacles);
            tile.SetObstacle(index);
        }

        private void SetupTargets()
        {
            TargetsDatas[] targetsDatas = player.GetTargetsData();
            player.SetupPlayerView();
            plateController.SetPhoto(player.HeroIndex);
            int targetsToFind = 3;
            List<int> indexes = new List<int>();

            for (int i = 0; i < spawnedTiles.Count; i++)
            {
                spawnedTiles[i].HideAllTargets();
                indexes.Add(i);
            }
            
            for (int i = 0; i < targetsToFind; i++)
            {
                int index = indexes[Random.Range(0, indexes.Count)];
                int targetIndex = Random.Range(0, spawnedTiles[index].NumberOfTargets);
                spawnedTiles[index].SetTargets(targetIndex, targetsDatas[i]);
                indexes.Remove(index);
            }
        }

        private void SetupRotation(Tile tile)
        {
            int rotation = Random.Range(0, 2);
            float eulerY = (rotation == 0) ? 0 : 180;
            tile.transform.localEulerAngles = new Vector3(0, eulerY, 0);
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void Start()
        {
            //SpawnTiles();
        }
    }
}