﻿
using krakjam2020.levelgenerator;
using System;
using UnityEngine.UI;

[Serializable]
public class LimbHUD
{
    public Image Sprite;
    public LimbType LimbType;
}
