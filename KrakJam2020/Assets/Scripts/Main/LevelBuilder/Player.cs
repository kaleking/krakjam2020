﻿namespace krakjam2020.levelgenerator {
    using UnityEngine;
    using UnityEngine.AddressableAssets;
    using UnityEngine.ResourceManagement.AsyncOperations;

    public class Player : MonoBehaviour
    {
        const string PATH = "heroes/Hero_{0}.prefab";
        [SerializeField] private GameObject playerRoot = null;
        public int heroeTypesCount = 5;
        private GameObject spawnedHero;
        private int actualHeroIndex;
        private TargetsDatas[] targetsDatas;

        public int HeroIndex => actualHeroIndex;

        public TargetsDatas[] GetTargetsData()
        {
            targetsDatas = new TargetsDatas[3];
            actualHeroIndex = Random.Range(0, heroeTypesCount);
            int armCount = Random.Range(1, 3);
            if (armCount == 1)
            {
                LimbType limbType = (Random.Range(0, 2) == 0) ? LimbType.LeftArm : LimbType.RightArm;
                targetsDatas[0] = new TargetsDatas() { limbType = limbType, HeroeIndex = actualHeroIndex };
                targetsDatas[1] = new TargetsDatas() { limbType = LimbType.LeftLeg, HeroeIndex = actualHeroIndex };
                targetsDatas[2] = new TargetsDatas() { limbType = LimbType.RightLeg, HeroeIndex = actualHeroIndex };
            }
            else
            {
                LimbType limbType = (Random.Range(0, 2) == 0) ? LimbType.LeftLeg : LimbType.RightLeg;
                targetsDatas[2] = new TargetsDatas() { limbType = limbType, HeroeIndex = actualHeroIndex };
                targetsDatas[0] = new TargetsDatas() { limbType = LimbType.LeftArm, HeroeIndex = actualHeroIndex };
                targetsDatas[1] = new TargetsDatas() { limbType = LimbType.RightArm, HeroeIndex = actualHeroIndex };
               
            }
            return targetsDatas;
        }

        public void SetupPlayerView()
        {
            Addressables.InstantiateAsync(string.Format(PATH, actualHeroIndex)).Completed += onInstantiated;
        }

        private void onInstantiated(AsyncOperationHandle<GameObject> obj)
        {
            if (spawnedHero != null)
            {
                Destroy(spawnedHero);
            }
            spawnedHero = obj.Result;
            spawnedHero.transform.parent = playerRoot.transform;
            spawnedHero.transform.localPosition = Vector3.zero;
            spawnedHero.transform.localScale = Vector3.one / 2;
            spawnedHero.transform.localRotation = Quaternion.identity;
            spawnedHero.transform.localEulerAngles = new Vector3(0, 180, 0);
            spawnedHero.transform.localPosition = new Vector3(0, -0.55f, 0);
            spawnedHero.GetComponent<HeroController>().SetupHero(targetsDatas);
            GetComponent<PathMover>().Rotate = true;
        }

    }
}