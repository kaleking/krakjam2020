﻿namespace krakjam2020.levelgenerator {
    using UnityEngine;
    using System.Collections;

    public class Target : MonoBehaviour
    {
        private TargetsDatas data;
        [SerializeField] TargetPartController Leg;
        [SerializeField] TargetPartController Arm;
        [SerializeField] Animator arrow;
        TargetPartController actual;
        public void SetupTarget(TargetsDatas data)
        {
            this.data = data;
            if (data.limbType == LimbType.LeftArm || data.limbType == LimbType.RightArm)
            {
                actual = Arm;
                Arm.SwitchOn(data.HeroeIndex);
            }
            else
            {
                actual = Leg;
                Leg.SwitchOn(data.HeroeIndex);
            }
        }

        private void OnTriggerEnter(Collider obj)
        {
            StartCoroutine(CloseArrow());
            GetComponent<Collider>().enabled = false;
            actual.SwitchOff();
            obj.GetComponentInChildren<HeroController>().AttachLimb(data.limbType);
        }

        private IEnumerator CloseArrow()
        {
            arrow.SetTrigger("close");
            yield return new WaitForSeconds(2f);
            arrow.gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            arrow.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            arrow.gameObject.SetActive(true);
        }
    }
}