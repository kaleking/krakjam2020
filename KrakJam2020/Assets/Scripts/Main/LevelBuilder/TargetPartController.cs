﻿namespace krakjam2020 {

    using UnityEngine;

    public class TargetPartController : MonoBehaviour {
        [SerializeField] GameObject[] IindexedObject = null;
        int actualIndex;
        public void SwitchOn(int index)
        {
            IindexedObject[index].SetActive(true);
            actualIndex = index;
        }

        public void SwitchOff()
        {
            IindexedObject[actualIndex].SetActive(false);
        }
    }
}