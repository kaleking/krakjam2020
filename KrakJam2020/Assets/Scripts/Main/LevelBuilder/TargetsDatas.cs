﻿using System;

namespace krakjam2020.levelgenerator
{
    public enum LimbType { LeftLeg, RightLeg, LeftArm, RightArm }
    [Serializable]
    public class TargetsDatas
    {
        public LimbType limbType;
        public int HeroeIndex;
    }
}