﻿namespace krakjam2020.levelgenerator {
    using System;
    using UnityEngine;

    public class Tile : MonoBehaviour {
        [SerializeField] GameObject[] environment = null;
        [SerializeField] GameObject[] obstacles = null;
        [SerializeField] GameObject[] targets = null;

        public int NumberOfEnvironments => environment.Length;
        public int NumberOfObstacles => obstacles.Length;
        public int NumberOfTargets => targets.Length;

        public void SetObstacle(int index)
        {
            SetupElements(obstacles, index);
        }

        public void SetTargets(int index, TargetsDatas data )
        {
            SetupElements(targets, index);
            targets[index].GetComponent<Target>().SetupTarget(data);
        }

        public void SetEnvironment(int index)
        {
            SetupElements(environment, index);
        }

        public void HideAllTargets()
        {
            SetupElements(targets, -1);
        }

        private void SetupElements(GameObject[] elements, int index)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                elements[i].SetActive(index == i);
            }
        }

        
    }
}