﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathCreator : MonoBehaviour
{
    [SerializeField] private float maxLength = 50f;
    private bool inRange;

    private LineRenderer lineRenderer;
    private List<Vector3> points = new List<Vector3>();
    public event Action<IEnumerable<Vector3>> OnNewPathCreated;
    private PathMover pathMover;
    private LineLengthIndicator lineLengthIndicator;

    private float lineLength = 0f;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        pathMover = FindObjectOfType<PathMover>();
        lineLengthIndicator = GetComponentInChildren<LineLengthIndicator>(true);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            points.Clear();
        }

        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                if (lineLengthIndicator.gameObject.activeSelf == false)
                {
                    lineLengthIndicator.gameObject.SetActive(true);
                } 
                
                if (lineLengthIndicator.MaxValue != maxLength)
                {
                    lineLengthIndicator.SetRange(maxLength);
                }

                float distance = DistanceToLastPoint(hitInfo.point);
                if (distance > .1f)
                {
                    points.Add(hitInfo.point);
                    lineRenderer.positionCount = points.Count;
                    lineRenderer.SetPositions(points.ToArray());
                    var pointsCount = points.Count;
                    if (points.Count > 1)
                    {
                        lineLength += (points[pointsCount - 1] - points[pointsCount - 2]).magnitude;
                        lineLengthIndicator.UpdateSlider(lineLength);
                    }
                    CheckIfLineLengthInRange();
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            TryCreatePath();
        }
    }

    public void ResetPath()
    {
        points.Clear();
        lineRenderer.positionCount = 0;
        lineLength = 0f;
        lineLengthIndicator.UpdateSlider(lineLength);
        OnNewPathCreated?.Invoke(points);
    }

    private void CheckIfLineLengthInRange()
    {
        if (pathMover.InNextTileRange(points.Last()) == true && lineLength <= maxLength)
        {
            SetLineColor(Color.green);
            inRange = true;
        }
        else if (lineLength < maxLength)
        {
            SetLineColor(Color.yellow);
            inRange = false;
        }
        else
        {
            SetLineColor(Color.red);
            inRange = false;
        }
    }


    private void TryCreatePath()
    {
        if (inRange == true)
        {
            OnNewPathCreated?.Invoke(points);
            pathMover.ResetTimer(false);
            pathMover.ShouldKill = false;
        }
        else
        {
            ResetPath();
        }
    }

    private float DistanceToLastPoint(Vector3 point)
    {
        if (!points.Any())
        {
            return Mathf.Infinity;
        }
        return Vector3.Distance(points.Last(), point);
    }

    private void SetLineColor(Color color)
    {
        lineRenderer.startColor = color;
        lineRenderer.endColor = color;
    }
}
