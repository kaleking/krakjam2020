﻿using krakjam2020;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathMover : MonoBehaviour
{
    [SerializeField] private Clock timerUI;
    [SerializeField] private float timerStartValue;
    private NavMeshAgent navMeshAgent;
    private Queue<Vector3> pathPoints = new Queue<Vector3>();
    private int currentTile = 0;
    private Vector3 initialPosition;
    private Animator animator;
    private PathCreator pathCreator;

    public bool Rotate { get; internal set; }

    private void Awake()
    {
        initialPosition = this.transform.position;
        animator = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.updateRotation = false;
        pathCreator = FindObjectOfType<PathCreator>();
        pathCreator.OnNewPathCreated += SetPoints;
    }    

    private void Start()
    {
        GameManager.Instance.OnGameOver += () =>
        {
            Rotate = false;
            transform.rotation = Quaternion.identity;
            transform.position = initialPosition;
        };

        GameManager.Instance.OnGameStart += () => ResetTimer(false);
    }

    private void SetPoints(IEnumerable<Vector3> points)
    {
        pathPoints = new Queue<Vector3>(points);
    }

    private void Update()
    {
        UpdatePathing();

        TrySwitchTile();

    }

    private void TrySwitchTile()
    {
        if (InNextTileRange(transform.position))
        {
            CheckIfCanGoFurther();
        }
    }

    private void CheckIfCanGoFurther()
    {
        // here insert a condition for picking missing body parts

        currentTile++;
        if (currentTile == 5)
        {
            var lastDistance = currentTile == 0 ? 15 : 12;
            CameraMover.Instance.MoveCamera(lastDistance);
            GetComponent<EndGameController>().EndGame();
            return;

        }
        
        var distance = currentTile == 0 ? 15 : 12;
        CameraMover.Instance.MoveCamera(distance);
        StartCoroutine(ResetNavMesh());
        ResetTimer();
    }

    public Coroutine TimerCoroutine;
    public bool ShouldKill;
    private IEnumerator Timer()
    {
        float timer = timerStartValue;
        while (timer > 0f)
        {
            timer -= Time.deltaTime;
            timerUI.UpdateClockValue(timer, timerStartValue);
            yield return null;
        }

        if (ShouldKill)
            GameManager.Instance.InvokeGameOver();
    }

    private IEnumerator ResetNavMesh()
    {
        navMeshAgent.isStopped = true;
        pathCreator.ResetPath();
        yield return new WaitForEndOfFrame();
        navMeshAgent.isStopped = false;
    }

    public bool InNextTileRange(Vector3 position)
    {
        return this.currentTile == 0 ? position.x <= 0 : position.x <= -(currentTile * 12);
    }

    private void FixedUpdate()
    {
        float speed = navMeshAgent.velocity.sqrMagnitude;
        float modifier = Mathf.Sin(Time.time/1.5f) * 25;
        if (speed > Mathf.Epsilon)
        {
            //Quaternion destinedRotation = Quaternion.LookRotation(navMeshAgent.velocity.normalized);
            //transform.rotation = Quaternion.Lerp(transform.rotation, destinedRotation, .1f);
           
        }

        if (Rotate)
            transform.Rotate(Vector3.up, Time.deltaTime*(speed+1)* modifier, Space.Self);
    }

    private void UpdatePathing()
    {
        if (ShouldSetDestination() && navMeshAgent.enabled)
        {
            navMeshAgent.SetDestination(pathPoints.Dequeue());
        }
    }

    public void ResetTimer(bool andStart = true)
    {
        timerUI.EnableClock();

        if (TimerCoroutine != null)
        {
            StopCoroutine(TimerCoroutine);
        }
        timerUI.UpdateClockValue(0, timerStartValue);
        ShouldKill = true;

        if (andStart == true)
        {
            timerUI.UpdateClockValue(timerStartValue, timerStartValue);
            TimerCoroutine = StartCoroutine(Timer());
        }
    }

    #region not used yet
    private IEnumerator SetDestination(Vector3 destinedPosition, Quaternion destinedRotation)
    {
        var step = 0f;
        while (step <= 1f)
        {
            step += Time.deltaTime;
            transform.rotation = Quaternion.Lerp(transform.rotation, destinedRotation, step);
            yield return null;
        }
        transform.rotation = destinedRotation;
    }
    #endregion

    private bool ShouldSetDestination()
    {
        if (pathPoints.Count == 0)
        {
            return false;
        }

        if (navMeshAgent.hasPath == false || navMeshAgent.remainingDistance < 0.3f)
        {
            return true;
        }

        return false;
    }

    public void Die()
    {
        StartCoroutine(DeathSequence());
    }

    private IEnumerator DeathSequence()
    {
        animator.SetTrigger("Die");
        navMeshAgent.enabled = false;
        yield return new WaitForSeconds(1f);
        this.transform.position = initialPosition;
        yield return new WaitForSeconds(1f);
        navMeshAgent.enabled = true;
        StartCoroutine(ResetNavMesh());
        currentTile = 0;
    }
}
