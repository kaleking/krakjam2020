﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer photo = null;
    [SerializeField] private Sprite[] photos;

    public void SetPhoto(int index)
    {
        photo.sprite = photos[index];
    }
}
