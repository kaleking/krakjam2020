﻿using krakjam2020.levelgenerator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour
{
    private Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => {
            LevelBuilder.Instance.seed = transform.parent.GetComponentInChildren<InputField>().text.ToString();
            LevelBuilder.Instance.SpawnTiles();            
            GameManager.Instance.InvokeGameStart();
        });
    }
}
