﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Log : MonoBehaviour
{
    [SerializeField] private Transform startPoint = null;
    [SerializeField] private Transform endPoint = null;
    private Animator animator;

    private Vector3 startPointCache;
    private Vector3 endPointCache;

    private Vector3 currentStartPoint;

    private bool isGoingForward = true;
    private float magnitude;

    private void Start()
    {
        animator = GetComponent<Animator>();
        startPointCache = startPoint.position;
        endPointCache = endPoint.position;
        SetForwardDirection();
        var destinationVector = endPointCache - startPointCache;
        magnitude = destinationVector.magnitude;
    }

    private void Update()
    {
        var heading = transform.position - currentStartPoint;     

        if (heading.magnitude > magnitude)
        {
            animator.SetBool("Drowned", isGoingForward);
            SetOppositeDirection();
        }
    }
    private void OnTriggerEnter(Collider player)
    {
        var pathMover = player.GetComponent<PathMover>();
        if (pathMover != null)
        {
            pathMover.Die();
        }
    }

    private void SetForwardDirection()
    {
        currentStartPoint = startPointCache;
        isGoingForward = true;
    }

    private void SetBackwardDirection()
    {
        currentStartPoint = endPointCache;
        isGoingForward = false;
    }

    private void SetOppositeDirection()
    {
        if (isGoingForward)
            SetBackwardDirection();
        else
            SetForwardDirection();
    }
}
