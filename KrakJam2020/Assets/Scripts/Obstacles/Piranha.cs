﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piranha : MonoBehaviour
{
    private void OnTriggerEnter(Collider player)
    {
        var pathMover = player.GetComponent<PathMover>();
        if (pathMover != null)
        {
            pathMover.Die();
        }
    }
}
