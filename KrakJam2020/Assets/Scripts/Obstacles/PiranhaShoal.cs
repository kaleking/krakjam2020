﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiranhaShoal : MonoBehaviour
{
    private Piranha[] piranhas;

    private void Awake()
    {
        piranhas = GetComponentsInChildren<Piranha>(true);
    }

    void Start()
    {
        StartCoroutine(InitiatePiranhaShoal());
    }

    private IEnumerator InitiatePiranhaShoal()
    {
        for (int i = 0; i < piranhas.Length; i++)
        {
            piranhas[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(1f);
        }
    }
}
