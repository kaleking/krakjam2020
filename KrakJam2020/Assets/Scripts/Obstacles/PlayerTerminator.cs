﻿using UnityEngine;

public class PlayerTerminator : MonoBehaviour
{
    private void OnTriggerEnter(Collider player)
    {
        var pathMover = player.GetComponent<PathMover>();
        if (pathMover != null)
        {
            pathMover.Die();
            GameManager.Instance.InvokeGameOver();
        }
    }
}
