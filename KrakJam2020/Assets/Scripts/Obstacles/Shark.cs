﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shark : MonoBehaviour, IObstacle
{
    [SerializeField] private Transform startPoint = null;
    [SerializeField] private Transform endPoint = null;
    private Animator animator;

    private Vector3 startPointCache;
    private Vector3 endPointCache;

    private Vector3 currentStartPoint;

    private bool isGoingForward = true;
    private float magnitude;

    private void Start()
    {
        animator = GetComponent<Animator>();
        startPointCache = startPoint.position;
        endPointCache = endPoint.position;
        SetForwardDirection();
        var destinationVector = endPointCache - startPointCache;
        magnitude = destinationVector.magnitude;
    }

    private void Update()
    {
        var heading = transform.position - currentStartPoint;

        if (heading.magnitude > magnitude)
        {
            animator.SetTrigger("Turn");
            SetOppositeDirection();
        }
    }

    public void SwitchOn()
    {
        this.gameObject.SetActive(true);
    }

    private void SetForwardDirection()
    {
        currentStartPoint = startPointCache;
        isGoingForward = true;
    }

    private void SetBackwardDirection()
    {
        currentStartPoint = endPointCache;
        isGoingForward = false;
    }

    private void SetOppositeDirection()
    {
        if (isGoingForward)
            SetBackwardDirection();
        else
            SetForwardDirection();
    }
}
