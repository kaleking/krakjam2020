﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Tide : MonoBehaviour
{
    private void OnTriggerEnter(Collider objectInRange)
    {
        var navMeshAgent = objectInRange.GetComponentInParent<NavMeshAgent>();
        if (navMeshAgent != null)
        {
            navMeshAgent.enabled = false;
        }
    }

    private void OnTriggerStay(Collider objectInRange)
    {
        var rigidbody = objectInRange.GetComponentInChildren<Rigidbody>();
        if (rigidbody != null)
        {
            rigidbody.AddForce(transform.forward * 15f, ForceMode.Force);
        }
    }
    private void OnTriggerExit(Collider objectInRange)
    {
        var navMeshAgent = objectInRange.GetComponentInParent<NavMeshAgent>();
        if (navMeshAgent != null)
        {
            navMeshAgent.enabled = true;
        }
    }
}
