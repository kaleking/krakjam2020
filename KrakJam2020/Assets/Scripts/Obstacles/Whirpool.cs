﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Whirpool : MonoBehaviour, IObstacle
{
    private Vector3 direction;
    public float forwardSpeed;
    public float spinningSpeed;

    private void OnTriggerEnter(Collider player)
    {
        var navMeshAgent = player.GetComponentInChildren<NavMeshAgent>();
        if (navMeshAgent != null)
        {
            player.GetComponentInChildren<Rigidbody>().isKinematic = true;
            navMeshAgent.enabled = false;
            StartCoroutine(PullInObject(player.transform));
        }
    }

    private IEnumerator PullInObject(Transform player)
    {
        float time = 0f;
        while (time <= 5f)
        {
            var direction = transform.position - player.position;
            Debug.DrawLine(player.position, transform.position);
            player.position = Vector3.MoveTowards(player.position, transform.position, Time.deltaTime * forwardSpeed);
            player.RotateAround(transform.position, Vector3.up, spinningSpeed * Time.deltaTime);
            time += Time.deltaTime;
            yield return null;
        }

        GameManager.Instance.InvokeGameOver();
    }

    public void SwitchOn()
    {
        this.gameObject.SetActive(true);
    }
}