﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SfxTrigger : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField] private AudioClip[] clips;

    public string Name;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySfx()
    {
        audioSource.PlayOneShot(clips[Random.Range(0, clips.Length)]);
    }
}
