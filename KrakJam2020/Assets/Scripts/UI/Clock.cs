﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    [SerializeField] private Image radialFill;
    [SerializeField] private Image pointer;
    [SerializeField] private Image clock;

    public void EnableClock()
    {
        radialFill.gameObject.SetActive(true);
        pointer.gameObject.SetActive(true);
        clock.gameObject.SetActive(true);
    }

    public void UpdateClockValue(float current, float max)
    {
        radialFill.fillAmount = current / max;
    }

    private void Update()
    {
        var rotation = pointer.transform.eulerAngles;
        rotation.z = -(radialFill.fillAmount/1) * 360;
        pointer.transform.eulerAngles = rotation;
    }
}
