﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineLengthIndicator : MonoBehaviour
{
    private Slider slider;
    [SerializeField] private Image sliderBackground = null;
    [SerializeField] private Color lengthWithinRange;
    [SerializeField] private Color approachingLimit;
    [SerializeField] private Color outOfRange;

    public float MaxValue { get; set; }

    private void Awake()
    {
        slider = GetComponent<Slider>();
    }

    private void OnEnable()
    {
        SetRange(MaxValue);
    }

    public void SetRange(float maxLength)
    {
        if (slider == null)
        {
            slider = GetComponent<Slider>();
        }
        slider.maxValue = maxLength;
    }

    public void UpdateSlider(float lineLength)
    {
        slider.value = lineLength;

        UpdateColors(lineLength);
    }

    private void UpdateColors(float lineLength)
    {
        if (lineLength <= slider.maxValue * .75f)
            sliderBackground.color = lengthWithinRange;
        else if (lineLength > slider.maxValue * .75f && lineLength < slider.maxValue)
            sliderBackground.color = approachingLimit;
        else
            sliderBackground.color = outOfRange;
    }
}
